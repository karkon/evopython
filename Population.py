import math
import random

import numpy as np

from Individual import Individual

"""Klasa reprezentujaca populacje osobnikow, ktora poddana bedzie dzialaniu algorytmu ewolucyjnego."""
class Population:
    def __init__(self, size=None, dimensions=None, minimum=None, maximum=None, fit_function=None, maximizing=None):
        """ """
        self.dimensions = dimensions
        """ """
        self.size = size
        """ """
        self.maximizing = maximizing
        """ """
        self.minimum = minimum
        """ """
        self.maximum = maximum
        """Lista osobnikow populacji"""
        self.individuals = []
        """ """
        self.fit_function = fit_function

    """Metoda umozliwiajaca generacje losowych osobnikow populacji."""
    def generate_random_population(self):
        self.individuals = [Individual(self.dimensions, self.minimum, self.maximum, self.fit_function) for _ in
                            range(self.size)]

    """Metoda odpowiedzialna za wykonanie krzyzowania na osobnikach rodzicielskich.
    
    Metoda przyjmuje osobniki rodzicielskie oraz parametr interpolacji. W rezultacie otrzymywany jest osobnik potomny."""
    def mate(self, parent1: Individual, parent2: Individual, param: float):
        new_values = []
        values = []
        deviations = []
        for i in range(self.dimensions): # w krzyzowaniu nalezy uwzglednic wszystkie wymiary danych osobnika
            # krzyzowanie wspolrzednych polozenia
            values.append(param * parent1.values[0][i] + (1 - param) * parent2.values[0][i])
            # krzyzowanie odchylen standardowych
            deviations.append(param * parent1.values[1][i] + (1 - param) * parent2.values[1][i])
        new_values.append(values)
        new_values.append(deviations)
        return Individual(self.dimensions,self.minimum,self.maximum,self.fit_function,new_values)

    """Metoda umozliwiajaca wykonanie turnieju dwuosobniczego na wylosowanych osobnikach populacji."""
    def tournament(self):
        parents = random.sample(self.individuals,2)
        if self.maximizing: # w przypadku problemu maksymalizacji osobnik lepszy to ten o wyzszej wartosci funkcji przystosowania
            if parents[0].fit > parents[1].fit:
                return parents[0]
            else:
                return parents[1]
        else: # w przypadku problemu minimalizacji osobnik lepszy to ten o nizszej wartosci funkcji przystosowania
            if parents[0].fit < parents[1].fit:
                return parents[0]
            else:
                return parents[1]

    """Metoda umozliwiajaca wykonanie mutacji osobnika (przesuniecie w przestrzeni).
    
    Metoda realizuje mutacje dla wariantu strategii ewolucyjnej mi=lambda.
    sigma_i = sigma_i*exp(t1*e1+t2*e2), gdzie 
    e1 - liczba losowa z rozkladu normalnego, jedna dla wszystkich wymiarow
    e2 - liczba losowa z rozkladu normalnego, rozna dla kazdego z wymiarow
    t1 = 1 / (math.sqrt(2 * wymiar))
    t2 = 1 / (math.sqrt(2 * math.sqrt(wymiar)))"""
    def mutate(self, new_individual: Individual):
       t1 = 1 / (math.sqrt(2 * self.dimensions))
       e1 = np.random.normal(0, 1, 1)
       t2 = 1 / (math.sqrt(2 * math.sqrt(self.dimensions)))
       for i in range(self.dimensions):
           e2 = np.random.normal(0, 1, 1)
           new_individual.values[1][i] = new_individual.values[1][i] * math.exp(t1 * e1 + t2 * e2)
       for i in range(self.dimensions):
           e2 = np.random.normal(0, 1, 1)
           # przesuniecie punktu w przestrzeni
           new_individual.values[0][i] = float(new_individual.values[0][i] + new_individual.values[1][i] * e2)
           new_individual.check_space_constraints() # sprawdzenie, czy punkty nie sa poza zakresem badan
       return new_individual

    """Metoda umozliwiajaca obliczenie wartosci funkcji przystosowania osobnikow populacji."""
    def evaluate(self):
        for i in range(self.individuals.__len__()):
            self.individuals[i].fit = self.individuals[i].evaluate(self.fit_function) #todo zostal tu koment: to chyba nie do konca jest ładne chyba by trzeba pochowac to bo  w sumie bardzo ingerujemy we wnetrze individuala

    """Metoda umozliwiajaca sortowanie osobnikow populacji wedlug ich wartosci funkcji przystosowania."""
    def sort_individuals(self):
        self.individuals = sorted(self.individuals, key=lambda individual: individual.fit)