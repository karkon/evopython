import numpy as np

from Individual import Individual
from Population import Population


class SingleEvolution:
    def __init__(self, size, dimensions, minimum, maximum, fit_function, maximizing, name=" ", mate_param=0.5): #todo przenazwac size
        self.dimensions = dimensions
        self.population = Population(size, dimensions, minimum, maximum, fit_function, maximizing)
        self.population.generate_random_population()
        self.step_count = 0
        self.best_fit = self.population.individuals[0].fit
        self.name = name
        self.max_step_count = round(10000 * dimensions / size)  # uzywane tylko w przypadku uruchomienia bez wysp
        self.expected_fit_function_minimum = fit_function * 100
        self.termination_error_value = 0.00000001
        self.error_print_steps = []
        self.error_print_index = 0
        self.calculate_print_steps()
        self.results = []
        self.mate_param = mate_param

    def perform_single_evolution(self):
        condition = True
        while self.step_count < self.max_step_count and condition:
            new_best_fit = self.single_evolution_step()
            condition = self.check_stop_conditions_2(condition, new_best_fit)
        self.results.append(self.step_count)
        self.results.append(self.best_fit - self.expected_fit_function_minimum)
        self.results.append(self.dimensions)
        self.results.append(self.population.fit_function)
        self.results.append(self.population.size)
        self.results.append(self.mate_param)
        return self.results

    def single_evolution_step(self):
        new_population = Population(self.population.size, self.population.dimensions, self.population.minimum,
                                    self.population.maximum,
                                    self.population.fit_function, self.population.maximizing)
        self.mate_all_population(new_population)
        new_population.evaluate()
        self.population.sort_individuals()
        new_population.sort_individuals()
        self.population, new_best_fit = self.replace_population(new_population)
        self.population.sort_individuals()
        self.step_count = self.step_count + 1
        # draw_plots(new_population.fit_function, new_population, self.dimensions, new_population.minimum,
        #            new_population.maximum, self.step_count, name= self.name)
        return new_best_fit

    def check_stop_conditions_2(self, condition, new_best_fit):
        if (round(new_best_fit, 9) - self.expected_fit_function_minimum) <= self.termination_error_value:
            condition = False
        self.best_fit = new_best_fit
        return condition

    def mate_all_population(self, new_population):
        for i in range(self.population.size):
            a = np.random.uniform(0, 1, 1)
            if a < self.mate_param:
                new_individual = self.population.mutate(
                    self.population.mate(self.population.tournament(), self.population.tournament(), 0.5))
            else:
                new_individual = self.population.mutate(self.population.tournament())
            new_population.individuals.append(new_individual)

    def replace_population(self, new_population):
        if self.population.maximizing:
            if self.population.individuals[self.population.size - 1].fit > new_population.individuals[
                self.population.size - 1].fit:
                new_population.individuals[0] = self.population.individuals[self.population.size - 1]
                best_fit = self.population.individuals[self.population.size - 1].fit
            else:
                best_fit = new_population.individuals[self.population.size - 1].fit
        else:
            if self.population.individuals[0].fit < new_population.individuals[0].fit:
                new_population.individuals[self.population.size - 1] = self.population.individuals[0]
                best_fit = self.population.individuals[0].fit
            else:
                best_fit = new_population.individuals[0].fit
        return new_population, best_fit

    def get_best_individual(self):
        travelling_individual: Individual
        if self.population.maximizing:
            travelling_individual = self.population.individuals[self.population.size - 1]
        else:
            travelling_individual = self.population.individuals[0]
        return travelling_individual

    def take_best_individual(self, travelling_individual: Individual):
        if self.population.maximizing:
            self.population.individuals[0] = travelling_individual
        else:
            self.population.individuals[self.population.size - 1] = travelling_individual

    def calculate_print_steps(self):
        self.error_print_steps.append(0.01 * self.max_step_count)
        self.error_print_steps.append(0.02 * self.max_step_count)
        self.error_print_steps.append(0.05 * self.max_step_count)
        self.error_print_steps.append(0.1 * self.max_step_count)
        self.error_print_steps.append(0.2 * self.max_step_count)
        self.error_print_steps.append(0.3 * self.max_step_count)
        self.error_print_steps.append(0.4 * self.max_step_count)
        self.error_print_steps.append(0.5 * self.max_step_count)
        self.error_print_steps.append(0.6 * self.max_step_count)
        self.error_print_steps.append(0.7 * self.max_step_count)
        self.error_print_steps.append(0.8 * self.max_step_count)
        self.error_print_steps.append(0.9 * self.max_step_count)
        self.error_print_steps.append(self.max_step_count)
