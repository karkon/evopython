import matplotlib.pyplot as plt
import numpy as np

from Population import Population
from cec17_functions import cec17_test_func


# todo poprawic wydajnosc reference plot

def draw_plots(func_num=None, population: Population = None, dimensions=None, minimum=None, maximum=None, step=None,
               name=None):
    if dimensions == 2:
        draw_reference_plots(func_num, population, dimensions, minimum, maximum, step, name)
    else:
        print("Brak mozliwosci narysowania wykresu ponieważ liczba wymiarów przekracza 2")


def draw_reference_plots(func_num=None, population: Population = None, dimensions=None, minimum=None, maximum=None,
                         step=None, name=None):
    mx = 20
    f = [0]
    z = 0
    y = []
    x = []
    values = []
    fit = []

    calculate_reference_values(dimensions, f, func_num, mx, x, y, z, minimum, maximum)

    x = np.array(x)
    y = np.array(y)

    extract_values_from_population(fit, population, values)
    # draw_3D_plot(fit, values, x, y)
    # plot_cross_sections(fit, values, x, y)
    plot_above_view(values, x, y, step, name)


def extract_values_from_population(fit, population, values):
    for i in range(0, population.individuals.__len__()):
        values.append(population.individuals[i].values[0])
        fit.append(population.individuals[i].fit)


def calculate_reference_values(dimensions, f, func_num, mx, x, y, z, minimum=None, maximum=None):
    for i in np.arange(minimum, maximum, 2):
        for j in np.arange(minimum, maximum, 2):
            random_list = [i, j]
            x.append(random_list)
            z = z + 1
    for i in range(0, z):
        cec17_test_func(x[i], f, dimensions, mx, func_num)  # 13 i 18 i 5 i 6
        y.extend(f)


def draw_3D_plot(fit, values, x, y):
    ax = plt.axes(projection='3d')
    ax.plot_trisurf(x[:, 0], x[:, 1], y, cmap='magma', edgecolor='none')
    ax.scatter(np.array(values)[:, 0], np.array(values)[:, 1], np.array(fit), color='red')
    plt.show()
    ax = plt.axes(projection='3d')
    ax.plot_trisurf(x[:, 1], x[:, 0], y, cmap='magma', edgecolor='none')
    ax.scatter(np.array(values)[:, 0], np.array(values)[:, 1], np.array(fit), color='red')
    plt.show()


def plot_above_view(values, x, y, step, name):
    ax = plt.axes()
    ax.scatter(x[:, 0], x[:, 1], c=y, cmap='magma')
    ax.scatter(np.array(values)[:, 0], np.array(values)[:, 1], color='red')
    plt.title("populacja " + str(name) + " krok " + str(step))
    plt.show()


def plot_cross_sections(fit, values, x, y):
    plt.plot(x[:, 0], y)
    plt.plot(np.array(values)[:, 0], np.array(fit), '.')
    plt.show()
    plt.plot(x[:, 1], y)
    plt.plot(np.array(values)[:, 1], np.array(fit), '.')
    plt.show()
