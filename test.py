import csv

from SingleEvolution import SingleEvolution

# single = SingleEvolution(100, 2, -100, 100, 5, False, name="1")
# single.perform_single_evolution()
#
# island = IslandEvolution(100, 10, -100, 100, 5, False, 3, 0.5)
# island.perform_evolution()

# for i in range(0, 50):
#     print("ITERACJA " + str(i) + "    ")
#     island = IslandEvolution(100, 10, -100, 100, 13, False, 10, 0.5)
#     island.perform_evolution()
########_________________________1
results = []
for i in range(0, 50):
    print("ITERACJA " + str(i) + "    ")
    single = SingleEvolution(100, 10, -100, 100, 5, False, name="1", mate_param=0.5)
    results.append(single.perform_single_evolution())
with open("zwykłe1-5.csv", "w") as f:
    wr = csv.writer(f)
    wr.writerows(results)

results = []
for i in range(0, 50):
    print("ITERACJA " + str(i) + "    ")
    single = SingleEvolution(100, 10, -100, 100, 6, False, name="1", mate_param=0.5)
    results.append(single.perform_single_evolution())
with open("zwykłe1-6.csv", "w") as f:
    wr = csv.writer(f)
    wr.writerows(results)

results = []
for i in range(0, 50):
    print("ITERACJA " + str(i) + "    ")
    single = SingleEvolution(100, 10, -100, 100, 13, False, name="1", mate_param=0.5)
    results.append(single.perform_single_evolution())
with open("zwykłe1-13.csv", "w") as f:
    wr = csv.writer(f)
    wr.writerows(results)

results = []
for i in range(0, 50):
    print("ITERACJA " + str(i) + "    ")
    single = SingleEvolution(100, 10, -100, 100, 18, False, name="1", mate_param=0.5)
    results.append(single.perform_single_evolution())
with open("zwykłe1-18.csv", "w") as f:
    wr = csv.writer(f)
    wr.writerows(results)
########_________________________2
results = []
for i in range(0, 50):
    print("ITERACJA " + str(i) + "    ")
    single = SingleEvolution(100, 10, -100, 100, 5, False, name="1", mate_param=0.25)
    results.append(single.perform_single_evolution())
with open("zwykłe2-5.csv", "w") as f:
    wr = csv.writer(f)
    wr.writerows(results)

results = []
for i in range(0, 50):
    print("ITERACJA " + str(i) + "    ")
    single = SingleEvolution(100, 10, -100, 100, 6, False, name="1", mate_param=0.25)
    results.append(single.perform_single_evolution())
with open("zwykłe2-6.csv", "w") as f:
    wr = csv.writer(f)
    wr.writerows(results)

results = []
for i in range(0, 50):
    print("ITERACJA " + str(i) + "    ")
    single = SingleEvolution(100, 10, -100, 100, 13, False, name="1", mate_param=0.25)
    results.append(single.perform_single_evolution())
with open("zwykłe2-13.csv", "w") as f:
    wr = csv.writer(f)
    wr.writerows(results)

results = []
for i in range(0, 50):
    print("ITERACJA " + str(i) + "    ")
    single = SingleEvolution(100, 10, -100, 100, 18, False, name="1", mate_param=0.25)
    results.append(single.perform_single_evolution())
with open("zwykłe2-18.csv", "w") as f:
    wr = csv.writer(f)
    wr.writerows(results)
########_________________________3
results = []
for i in range(0, 50):
    print("ITERACJA " + str(i) + "    ")
    single = SingleEvolution(100, 10, -100, 100, 5, False, name="1", mate_param=0.75)
    results.append(single.perform_single_evolution())
with open("zwykłe3-5.csv", "w") as f:
    wr = csv.writer(f)
    wr.writerows(results)

results = []
for i in range(0, 50):
    print("ITERACJA " + str(i) + "    ")
    single = SingleEvolution(100, 10, -100, 100, 6, False, name="1", mate_param=0.75)
    results.append(single.perform_single_evolution())
with open("zwykłe3-6.csv", "w") as f:
    wr = csv.writer(f)
    wr.writerows(results)

results = []
for i in range(0, 50):
    print("ITERACJA " + str(i) + "    ")
    single = SingleEvolution(100, 10, -100, 100, 13, False, name="1", mate_param=0.75)
    results.append(single.perform_single_evolution())
with open("zwykłe3-13.csv", "w") as f:
    wr = csv.writer(f)
    wr.writerows(results)

results = []
for i in range(0, 50):
    print("ITERACJA " + str(i) + "    ")
    single = SingleEvolution(100, 10, -100, 100, 18, False, name="1", mate_param=0.75)
    results.append(single.perform_single_evolution())
with open("zwykłe3-18.csv", "w") as f:
    wr = csv.writer(f)
    wr.writerows(results)
########_________________________4
results = []
for i in range(0, 50):
    print("ITERACJA " + str(i) + "    ")
    single = SingleEvolution(100, 10, -100, 100, 5, False, name="1", mate_param=1.1)
    results.append(single.perform_single_evolution())
with open("zwykłe4-5.csv", "w") as f:
    wr = csv.writer(f)
    wr.writerows(results)

results = []
for i in range(0, 50):
    print("ITERACJA " + str(i) + "    ")
    single = SingleEvolution(100, 10, -100, 100, 6, False, name="1", mate_param=1.1)
    results.append(single.perform_single_evolution())
with open("zwykłe4-6.csv", "w") as f:
    wr = csv.writer(f)
    wr.writerows(results)

results = []
for i in range(0, 50):
    print("ITERACJA " + str(i) + "    ")
    single = SingleEvolution(100, 10, -100, 100, 13, False, name="1", mate_param=1.1)
    results.append(single.perform_single_evolution())
with open("zwykłe4-13.csv", "w") as f:
    wr = csv.writer(f)
    wr.writerows(results)

results = []
for i in range(0, 50):
    print("ITERACJA " + str(i) + "    ")
    single = SingleEvolution(100, 10, -100, 100, 18, False, name="1", mate_param=1.1)
    results.append(single.perform_single_evolution())
with open("zwykłe4-18.csv", "w") as f:
    wr = csv.writer(f)
    wr.writerows(results)
########_________________________5
results = []
for i in range(0, 50):
    print("ITERACJA " + str(i) + "    ")
    single = SingleEvolution(50, 10, -100, 100, 5, False, name="1", mate_param=0.5)
    results.append(single.perform_single_evolution())
with open("zwykłe5-5.csv", "w") as f:
    wr = csv.writer(f)
    wr.writerows(results)

results = []
for i in range(0, 50):
    print("ITERACJA " + str(i) + "    ")
    single = SingleEvolution(50, 10, -100, 100, 6, False, name="1", mate_param=0.5)
    results.append(single.perform_single_evolution())
with open("zwykłe5-6.csv", "w") as f:
    wr = csv.writer(f)
    wr.writerows(results)

results = []
for i in range(0, 50):
    print("ITERACJA " + str(i) + "    ")
    single = SingleEvolution(50, 10, -100, 100, 13, False, name="1", mate_param=0.5)
    results.append(single.perform_single_evolution())
with open("zwykłe5-13.csv", "w") as f:
    wr = csv.writer(f)
    wr.writerows(results)

results = []
for i in range(0, 50):
    print("ITERACJA " + str(i) + "    ")
    single = SingleEvolution(50, 10, -100, 100, 18, False, name="1", mate_param=0.5)
    results.append(single.perform_single_evolution())
with open("zwykłe5-18.csv", "w") as f:
    wr = csv.writer(f)
    wr.writerows(results)
########________________________6
results = []
for i in range(0, 50):
    print("ITERACJA " + str(i) + "    ")
    single = SingleEvolution(50, 10, -100, 100, 5, False, name="1", mate_param=0.25)
    results.append(single.perform_single_evolution())
with open("zwykłe6-5.csv", "w") as f:
    wr = csv.writer(f)
    wr.writerows(results)

results = []
for i in range(0, 50):
    print("ITERACJA " + str(i) + "    ")
    single = SingleEvolution(50, 10, -100, 100, 6, False, name="1", mate_param=0.25)
    results.append(single.perform_single_evolution())
with open("zwykłe6-6.csv", "w") as f:
    wr = csv.writer(f)
    wr.writerows(results)

results = []
for i in range(0, 50):
    print("ITERACJA " + str(i) + "    ")
    single = SingleEvolution(50, 10, -100, 100, 13, False, name="1", mate_param=0.25)
    results.append(single.perform_single_evolution())
with open("zwykłe6-13.csv", "w") as f:
    wr = csv.writer(f)
    wr.writerows(results)

results = []
for i in range(0, 50):
    print("ITERACJA " + str(i) + "    ")
    single = SingleEvolution(50, 10, -100, 100, 18, False, name="1", mate_param=0.25)
    results.append(single.perform_single_evolution())
with open("zwykłe6-18.csv", "w") as f:
    wr = csv.writer(f)
    wr.writerows(results)
########_______________________7
results = []
for i in range(0, 50):
    print("ITERACJA " + str(i) + "    ")
    single = SingleEvolution(50, 10, -100, 100, 5, False, name="1", mate_param=0.75)
    results.append(single.perform_single_evolution())
with open("zwykłe7-5.csv", "w") as f:
    wr = csv.writer(f)
    wr.writerows(results)

results = []
for i in range(0, 50):
    print("ITERACJA " + str(i) + "    ")
    single = SingleEvolution(50, 10, -100, 100, 6, False, name="1", mate_param=0.75)
    results.append(single.perform_single_evolution())
with open("zwykłe7-6.csv", "w") as f:
    wr = csv.writer(f)
    wr.writerows(results)

results = []
for i in range(0, 50):
    print("ITERACJA " + str(i) + "    ")
    single = SingleEvolution(50, 10, -100, 100, 13, False, name="1", mate_param=0.75)
    results.append(single.perform_single_evolution())
with open("zwykłe7-13.csv", "w") as f:
    wr = csv.writer(f)
    wr.writerows(results)

results = []
for i in range(0, 50):
    print("ITERACJA " + str(i) + "    ")
    single = SingleEvolution(50, 10, -100, 100, 18, False, name="1", mate_param=0.75)
    results.append(single.perform_single_evolution())
with open("zwykłe7-18.csv", "w") as f:
    wr = csv.writer(f)
    wr.writerows(results)
########_______________________8
results = []
for i in range(0, 50):
    print("ITERACJA " + str(i) + "    ")
    single = SingleEvolution(50, 10, -100, 100, 5, False, name="1", mate_param=1.1)
    results.append(single.perform_single_evolution())
with open("zwykłe8-5.csv", "w") as f:
    wr = csv.writer(f)
    wr.writerows(results)

results = []
for i in range(0, 50):
    print("ITERACJA " + str(i) + "    ")
    single = SingleEvolution(50, 10, -100, 100, 6, False, name="1", mate_param=1.1)
    results.append(single.perform_single_evolution())
with open("zwykłe8-6.csv", "w") as f:
    wr = csv.writer(f)
    wr.writerows(results)

results = []
for i in range(0, 50):
    print("ITERACJA " + str(i) + "    ")
    single = SingleEvolution(50, 10, -100, 100, 13, False, name="1", mate_param=1.1)
    results.append(single.perform_single_evolution())
with open("zwykłe8-13.csv", "w") as f:
    wr = csv.writer(f)
    wr.writerows(results)

results = []
for i in range(0, 50):
    print("ITERACJA " + str(i) + "    ")
    single = SingleEvolution(50, 10, -100, 100, 18, False, name="1", mate_param=1.1)
    results.append(single.perform_single_evolution())
with open("zwykłe8-18.csv", "w") as f:
    wr = csv.writer(f)
    wr.writerows(results)
########################### A TERAZ DLA 30
